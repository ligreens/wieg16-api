<?php
require 'database.php';
$id = $_GET['company_id'];
$sql = "SELECT * FROM customer WHERE company_id = :id ";
$row = $pdo->prepare($sql);
$row->execute([':id' => $id]);
$data = $row->fetchAll();
echo json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);