<?php require 'database.php';
//wieg16-api.dev/customers.php?id=1&street=true
$id = $_GET['id'];
$street = $_GET['street'];
$sql = "SELECT street, city, postcode FROM customer_address  WHERE customer_id = :id";
$row = $pdo->prepare($sql);
$row->execute([':id' => $id]);
if ($row->rowCount() > 0 && isset($street) && $street == true) {
$result = $row->fetchAll();
$main = array('data' => $result);
echo json_encode($main, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
}
else {
    header("HTTP/1.0 404 Not Found");
echo json_encode(["message" => "Customer address not found"]);

}